#!/usr/bin/env python3

# Based on the following works:
# - original script by: Will Yager
# 	http://yager.io/LEDStrip/LED.html
# - mod by Different55 <burritosaur@protonmail.com>
#
# ... finally refactored and modified by Mahe (mahe@fsfe.org)

import pyaudio as pa
import numpy as np
import sys
from blinkstick import blinkstick
from lib.audio_processing import process as audio_proc
from time import sleep, time
from lib import common
from colorsys import hsv_to_rgb, rgb_to_hsv

"""
AUDIO SPECTRUM VISUALIZER FOR BLINKSTICK FLEX (LINUX, PULSEAUDIO)

This script *optionally* takes a base color in hex notation (e.g. '#ff0000').
If specified, only this color will be used and only volume is visualized.
If no color is specified, the color will be determined based on the sound
frequency, making it more colorful and representative of the sound pattern.

In order for this to work correctly do the following once:
1. start this script
2. open up the PulseAudio sound control (pavucontrol)
3. select the tab "Recording"
4. identify the entry belonging to this script (usually called
   "ALSA plug-in [python3.X]")
5. at the right of the entry select the correct "Monitor of ..." from
   the dropdown field that matches your default output device (e.g. speakers)

After this procedure, the script should record the sound that is being emitted
from the output device and visualize its audio spectrum. Per default, PulseAudio
should remember this choice across reboots.
"""

# Whether or not to pulse to/from both ends of the strip
TWO_SIDES = True

# Sensitivity to sound
SOUND_SENSITIVITY = 1.3

# Limit maximum brightness
MAX_BRIGHTNESS = 0.5  # [0.0 ... 1.0]

# invert the direction of the pulse?
INVERT_DIRECTION = True


audio_stream = pa.PyAudio().open(format=pa.paInt16, \
								channels=2, \
								rate=44100, \
								input=True, \
								# Uncomment and set this if device differs
								#input_device_index=2, \
								frames_per_buffer=1024)


# Convert the audio data to numbers, num_samples at a time.
def read_audio(audio_stream, num_samples):
	while True:
		# Read all the input data. 
		samples = audio_stream.read(num_samples) 
		# Convert input data to numbers
		samples = np.fromstring(samples, dtype=np.int16).astype(np.float)
		samples_l = samples[::2]  
		samples_r = samples[1::2]
		yield samples_l, samples_r


stick = blinkstick.find_first()
count = stick.get_led_count()


def send_to_stick(strip):
	stick.set_led_data(0, strip)


if __name__ == '__main__':

	audio = read_audio(audio_stream, num_samples=1024)
	leds = audio_proc(audio, num_leds=32, num_samples=1024,
					  sample_rate=44100, sensitivity=SOUND_SENSITIVITY)
	
	if TWO_SIDES:
		data = [0]*int(count/2)*3
		data2 = [0]*int(count/2)*3
	else:
		data = [0]*count*3

	predefined_hue = None
	if len(sys.argv) > 1:
		hex = sys.argv[1]
		predefined_hue, _, _ = rgb_to_hsv(*common.hex_to_rgb(hex))
	
	sent = 0
	try:
		for frame in leds:
			brightest = 0
			for i, led in enumerate(frame):
				if led > frame[brightest]:
					brightest = i

			hue = predefined_hue if predefined_hue else brightest/48.0

			color = hsv_to_rgb(hue, 1, min(frame[brightest]*1.2, 1))

			del data[-1]
			del data[-1]
			del data[-1] # I feel dirty having written this.
			if TWO_SIDES:
				del data2[0]
				del data2[0]
				del data2[0]

			color = [int(color[1]*255*MAX_BRIGHTNESS),
					 int(color[0]*255*MAX_BRIGHTNESS),
					 int(color[2]*255*MAX_BRIGHTNESS)]

			data = color + data

			if TWO_SIDES:
				data2 = data2 + color
				finaldata = data2+data if INVERT_DIRECTION else data+data2
			else:
				finaldata = data

			now = time()
			if now-sent < .02:
				sleep(max(0, .02-(now-sent)))

			sent = time()
			send_to_stick(finaldata)

	except KeyboardInterrupt:
		print("Interrupt. Exiting after turning off BlinkStick ...")
		stick.set_led_data(0, [0, 0, 0] * count)
		sys.exit(0)
