# BlinkStick Flex scripts

Python scripts for various effects using BlinkStick Flex LED strips on Linux.

## Repository Contents

| Script | Description | Comment |
|---|---|---|
| `ambilight.py` | Horizontal Ambilight clone | Custom one-dimensional horizontal lighting according to live screen capture, for Linux X11. |
| `music.py` | Audio spectrum visualizer | Based on Will Yager's and Different55's works. Optionally allows specfiying a color in hex format to make in mono-colored. |
| `waves.py` | Random pulsing animation | Allows specifying one or more colors. A random gradient with varying brightness, hue and saturation shifts based on the color(s) is generated for each iteration and faded into. |

All scripts include configurable options which can be found at the top of the code in uppercase notation.