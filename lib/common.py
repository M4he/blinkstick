import math
import numpy


def get_colors_from_image(pil_image, num_colors, top_offset=0):
    step_size = int(math.floor(pil_image.width/num_colors))
    y_coord = int(math.floor(pil_image.height*top_offset))
    ret = []
    for i in range(0, num_colors):
        x_coord = i*step_size
        ret.append(pil_image.getpixel((x_coord, y_coord)))
    return ret


def hex_to_rgb(hex):
    color = hex.lstrip('#')
    return tuple(int(color[i:i + 2], 16) / 255 for i in (0, 2, 4))


def fade_color(old_color, new_color, factor):
    """
    Fades an old_color into a new_color
    whereby the factor determines the percentage
    of how much to fade into the new_color;
    0.0 = old_color, 1.0 = new_color
    """
    color = numpy.array(old_color)
    target = numpy.array(new_color)
    vector = target - color
    return tuple(color + vector * factor)
