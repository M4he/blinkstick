#!/usr/bin/env python3
import sys
import time
import math
import numpy
import random
from lib import common
from PIL import Image
import cairo
from colorsys import hsv_to_rgb, rgb_to_hsv

from blinkstick import blinkstick

"""
AMBIENT COLOR WAVE PULSING FOR BLINKSTICK FLEX

This script takes one or more base colors in hex notation (e.g. '#ff0000') and
generates random color gradients based on those colors according to the settings
below and illuminates the LED strip of the BlinkStick according to the gradient.

The gradient has randomized brightness and hue shifts, smoothly generating
pulsing waves around the specified base colors.
"""

# How often (at most) is the BlinkStick fed with new data per second
# (as documented elsewhere, 50 fps seems to be the maximum it can handle)
BLINKSTICK_FPS = 50

# My BlinkStick tends to have very blueish whites when compared to my
# sRGB screen, so we need to account for the difference in whitepoint.
WHITEPOINT_COMPENSATION = (1.0, 0.85, 0.6)  # (R, G, B) scale ratios

# After how many frames of the BLINKSTICK_FPS should the color gradient change?
# Higher values will make the animation slower and smoother, lower values make
# the animation more hectic.
COLOR_BLENDING_FRAMES = 41

# Color gradient settings
MAX_HUE_SHIFT = 10   # [0 .. 1000], adds randomized hue shift
MAX_SAT_SHIFT = 25  # [0 .. 1000], adds randomized saturation shift
MIN_GRADIENT_STOPS = 3  # [1 .. N], minimum of gradient stops in the middle
MAX_GRADIENT_STOPS = 4  # [MIN_GRADIENT_STOPS .. N], maximum gradient stops
MIN_GRADIENT_BRIGHTNESS = 25  # [0 ... 100]
MAX_GRADIENT_BRIGHTNESS = 50  # [MIN_GRADIENT_BRIGHTNESS ... 100]


def generate_gradient(base_colors, width=32):

    def randomize_color():
        def limit_float(f):
            return max(0.0, min(1.0, f))

        intensity = limit_float(random.randint(MIN_GRADIENT_BRIGHTNESS,
                                               MAX_GRADIENT_BRIGHTNESS)/100)
        hue_shift = random.randint(-MAX_HUE_SHIFT, MAX_HUE_SHIFT)/1000
        sat_shift = random.randint(-MAX_SAT_SHIFT, MAX_SAT_SHIFT)/1000
        base_color = base_colors[random.randint(0, len(base_colors)-1)]
        h, s, v = rgb_to_hsv(*base_color)
        hue = limit_float(h + hue_shift)
        sat = limit_float(s + sat_shift)
        return hsv_to_rgb(hue, sat, intensity)

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, 1)
    ctx = cairo.Context(surface)

    # gradient pattern
    gradient_stops = random.randint(2+MIN_GRADIENT_STOPS, 2+MAX_GRADIENT_STOPS)
    pattern = cairo.LinearGradient(0, 0, width, 1)
    for i in range(gradient_stops):
        x_coord = i / (gradient_stops-1)
        r, g, b = randomize_color()
        pattern.add_color_stop_rgb(x_coord, r, g, b)

    ctx.set_source(pattern)
    ctx.rectangle(0, 0, width, 1)
    ctx.fill()

    # TODO: make this better (don't use ARGB at all)
    # from: https://www.casualhacker.net/post/2013-06-02-convert-pycairo-argb32-surface-to-pil-rgb-image/
    argbArray = numpy.frombuffer(bytes(surface.get_data()), 'c').reshape(-1, 4)
    rgbArray = argbArray[:, 2::-1]
    pilData = rgbArray.reshape(-1).tostring()

    pil = Image.frombuffer('RGB', (width, 1), pilData, 'raw', 'RGB', 0, 1)
    return pil


class WavePulsing(object):

    def __init__(self):
        stk = blinkstick.find_first()
        if not stk:
            print("No BlinkStick found, aborting ...")
            sys.exit(1)
        self.bstick = stk
        self.led_count = stk.get_led_count()

        # initialize colors to black
        stk.set_led_data(0, [0, 0, 0] * self.led_count)
        self.old_colors = [(0, 0, 0)] * self.led_count
        self.new_colors = [(0, 0, 0)] * self.led_count

    def apply_colors(self, colors):
        data = []
        for color in colors:
            data.append(int(math.floor(
                color[1] * WHITEPOINT_COMPENSATION[1]
            )))  # G
            data.append(int(math.floor(
                color[0] * WHITEPOINT_COMPENSATION[0]
            )))  # R
            data.append(int(math.floor(
                color[2] * WHITEPOINT_COMPENSATION[2]
            )))  # B
        self.bstick.set_led_data(0, data)

    def generate_faded_colors(self, ratio):
        ret = []
        for i, new_color in enumerate(self.new_colors):
            old_color = self.old_colors[i]
            ret.append(common.fade_color(old_color, new_color, ratio))
        return ret

    def main(self, base_colors):
        try:
            current_frame = 0
            grab_after_frames = COLOR_BLENDING_FRAMES + 1
            print("Rendering %s frames per second." % BLINKSTICK_FPS)
            print("Changing gradient after each %s frames." % grab_after_frames)
            while True:
                current_frame += 1
                if current_frame == grab_after_frames:
                    gradient = generate_gradient(base_colors)
                    self.old_colors = self.new_colors
                    self.new_colors = common.get_colors_from_image(gradient,
                                        self.led_count)
                    current_frame = 0

                frame_fade_ratio = float(current_frame / COLOR_BLENDING_FRAMES)
                if frame_fade_ratio < 1.0:
                    led_colors = self.generate_faded_colors(frame_fade_ratio)
                else:
                    led_colors = self.generate_faded_colors(1.0)
                self.apply_colors(led_colors)

                time.sleep(1.0/BLINKSTICK_FPS)

        except KeyboardInterrupt:
            print("Interrupt. Exiting after turning off BlinkStick ...")
            self.bstick.set_led_data(0, [0, 0, 0] * self.led_count)
            sys.exit(0)

assert (len(sys.argv) > 1), "Please specify at least one color in hex format!"
colors = [common.hex_to_rgb(col) for col in sys.argv[1:]]
wv = WavePulsing()
wv.main(colors)
