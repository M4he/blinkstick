#!/usr/bin/env python3
import sys
import math
import time
import cv2
import numpy
from PIL import Image
from lib import common

# GTK 3
import gi
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk

from blinkstick import blinkstick

"""
ONE-DIMENSIONAL HORIZONTAL AMBILIGHT FOR BLINKSTICK FLEX (LINUX, X11)

This is a Python implementation of an Ambilight clone for the BlinkStick Flex.
It utilizes OpenCV, Numpy and GTK3 to capture the screen and pick representative
colors for the LED strip in order to match the screen contents when the
BlinkStick is mounted horizontally behind the screen.

Each screen capture is heavily blurred using OpenCV and then colors are picked
from that blurred screen representation according to the amount of LEDs on the
strip.
"""

# Inverts the horizontal orientation when sending data to the BlinkStick
# inverted means that the USB connector is at the right side
BLINKSTICK_INVERT = True

# How often (at most) is the BlinkStick fed with new data per second
# (as documented elsewhere, 50 fps seems to be the maximum it can handle)
BLINKSTICK_FPS = 50

# How many frames of the BLINKSTICK_FPS should be consumed to fade colors
# into each other. More frames means smoother transitions but increased
# delay of updating the colors.
COLOR_BLENDING_FRAMES = 5  # [1 .. BLINKSTICK_FPS/SCREEN_GRABS_PER_SECOND]

# How often should the screen be captured and analyzed per second.
# Increasing this directly increases the resource and CPU usage.
SCREEN_GRABS_PER_SECOND = 6

# My BlinkStick tends to have very blueish whites when compared to my
# sRGB screen, so we need to account for the difference in whitepoint.
WHITEPOINT_COMPENSATION = (1.0, 0.85, 0.6)  # (R, G, B) scale ratios

# Limit the maximum brightness of the BlinkStick LEDs when sending
# colors to it.
MAX_BRIGHTNESS = 0.65  # [0.0 ... 1.0]

# Defines the blur kernel size. Since we have a one-dimensional sampling
# only in horizontal direction, we need to squash the vertical space a bit
# more than the horizontal one to account for the loss in dimension by including
# more blur samples in vertical direction.
BLUR_SIZE_HORIZONTAL = 100
BLUR_SIZE_VERTICAL = 150

# Defines at which part of the screen in vertical direction (Y coordinate)
# the colors are picked from. Represents the percentage of the screen height
# starting from the top, i.e. 0.0 = top screen edge, 1.0 = bottom screen edge.
Y_GRAB_OFFSET_FROM_TOP = 0.85  # [0.0 ... 1.0]


# Sanity check
assert (COLOR_BLENDING_FRAMES <=
        math.floor(BLINKSTICK_FPS/SCREEN_GRABS_PER_SECOND)), \
        "COLOR_BLENDING_FRAMES must not be greater than the frames until the " \
        "next screengrab (= BLINKSTICK_FPS / SCREEN_GRABS_PER_SECOND)!"


def pil_image_to_opencv(pil_image):
    numpy_image = numpy.array(pil_image)
    return cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)


def opencv_image_to_pil(cv_image):
    color_converted = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
    return Image.fromarray(color_converted)


def opencv_apply_blur(cv_image, border_type=cv2.BORDER_DEFAULT):
    ret = cv2.blur(cv_image, (BLUR_SIZE_HORIZONTAL, BLUR_SIZE_VERTICAL),
                   border_type)
    return ret


class OneDimensionalAmbilight(object):

    def __init__(self):
        self.window = Gdk.get_default_root_window()
        root_size = self.window.get_geometry()
        self.w = root_size.width
        self.h = root_size.height

        stk = blinkstick.find_first()
        if not stk:
            print("No BlinkStick found, aborting ...")
            sys.exit(1)
        self.bstick = stk
        self.led_count = stk.get_led_count()

        # initialize colors to black
        stk.set_led_data(0, [0, 0, 0] * self.led_count)
        self.old_colors = [(0, 0, 0)] * self.led_count
        self.new_colors = [(0, 0, 0)] * self.led_count

    def get_screenshot(self):
        pixbuf = Gdk.pixbuf_get_from_window(self.window, 0, 0, self.w, self.h)
        image = Image.frombuffer('RGB', (self.w, self.h), pixbuf.get_pixels(),
                                 'raw', 'RGB', pixbuf.get_rowstride(), 1)
        return image

    def generate_faded_colors(self, ratio):
        ret = []
        for i, new_color in enumerate(self.new_colors):
            old_color = self.old_colors[i]
            ret.append(common.fade_color(old_color, new_color, ratio))
        return ret

    def apply_colors(self, colors):
        data = []
        if BLINKSTICK_INVERT:
            colors.reverse()
        for color in colors:
            data.append(int(round(
                color[1] * MAX_BRIGHTNESS * WHITEPOINT_COMPENSATION[1]
            )))  # G
            data.append(int(round(
                color[0] * MAX_BRIGHTNESS * WHITEPOINT_COMPENSATION[0]
            )))  # R
            data.append(int(round(
                color[2] * MAX_BRIGHTNESS * WHITEPOINT_COMPENSATION[2]
            )))  # B
        self.bstick.set_led_data(0, data)

    def main(self):
        try:
            current_frame = 0
            grab_after_frames = int(math.floor(
                (1.0/SCREEN_GRABS_PER_SECOND) / (1.0/BLINKSTICK_FPS)
            ))
            print("Updating BlinkStick at %s frames per second." % BLINKSTICK_FPS)
            print("Grabbing new screenshot after each %s frames." % grab_after_frames)
            while True:
                current_frame += 1
                if current_frame == grab_after_frames:
                    scr = self.get_screenshot()
                    ocv = pil_image_to_opencv(scr)
                    ocv = opencv_apply_blur(ocv)
                    scr = opencv_image_to_pil(ocv)
                    self.old_colors = self.new_colors
                    self.new_colors = common.get_colors_from_image(scr,
                                                                   self.led_count,
                                                                   Y_GRAB_OFFSET_FROM_TOP)
                    current_frame = 0

                frame_fade_ratio = float(current_frame / COLOR_BLENDING_FRAMES)
                if frame_fade_ratio < 1.0:
                    colors = self.generate_faded_colors(frame_fade_ratio)
                else:
                    colors = self.generate_faded_colors(1.0)
                self.apply_colors(colors)

                time.sleep(1.0/BLINKSTICK_FPS)

        except KeyboardInterrupt:
            print("Interrupt. Exiting after turning off BlinkStick ...")
            self.bstick.set_led_data(0, [0, 0, 0] * self.led_count)
            sys.exit(0)


al = OneDimensionalAmbilight()
al.main()
